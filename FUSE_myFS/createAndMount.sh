#!/bin/bash
#@author: Francisco Jose Lozano Serrano

if [ ! -d mount-point ]; then
        mkdir mount-point;
fi

gnome-terminal -e "./fs-fuse -t 2097152 -a virtual-disk -f '-d -s mount-point'" 
