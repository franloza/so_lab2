#!/bin/bash
#@author: Francisco Jose Lozano Serrano

MPOINT="./mount-point"
VDISK="./virtual-disk"
MY_FSCK="../my-fsck/my-fsck"

#Colors
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
RED=`tput setaf 1`
END_COLOR=`tput sgr0`

if [ ! -d $MPOINT ]; then
        echo -e "${RED}$MPOINT does not exist!${END_COLOR}"
        exit 1
fi

#Copy two text files with size greater than 1 block (e.g., fuseLib.c and myFS.h) into the
#FS as well as into a temp directory in the Linux's file system.

echo -e "\nCopying fuseLib.c and myFS.h to $MPOINT"
cp ./src/fuseLib.c ./src/myFS.h $MPOINT 

echo -e "Copying fuseLib.c and myFS.h to temp directory\n"

if [ -d temp ]; then
        rm -rf ./temp
fi

mkdir temp

cp ./src/fuseLib.c ./src/myFS.h ./temp

#Check the integrity of the virtual disk with my-fsck and perform a diff between the
#original files and those copied to the FS.

echo -e "Checking the integrity of the virtual disk\n"
if  $MY_FSCK $VDISK; then
	echo -e "${GREEN}The integrity of the virtual disk is OK${END_COLOR}";

else
        echo -e "${RED}The integrity of the virtual disk is not OK${END_COLOR}";
fi

read -p "Press enter..."

DIFF1=$(diff $MPOINT/fuseLib.c ./temp/fuseLib.c -s)
DIFF2=$(diff $MPOINT/myFS.h ./temp/myFS.h -s)

#Compare the contents of each extracted file with the associated
#original file found in the parent directory.

echo -e "${YELLOW}COMPARISON BETWEEN FILES OF EACH FS: \n$DIFF1 \n$DIFF2 ${END_COLOR}\n"

#Truncate the first file in the temp folder and in your FS so as to 
#reduce the file size in one block.

echo -e "Reducing the size in one block of fuseLib.c in $MPOINT and in the temp directory\n"

SIZEBLOCK_FUSE=$(stat -c "%o" $MPOINT/fuseLib.c)
SIZEBLOCK_LINUX=$(stat -c "%o" ./temp/fuseLib.c)
SIZEFILE_FUSE=$(stat -c "%s" $MPOINT/fuseLib.c)
SIZEFILE_LINUX=$(stat -c "%s" temp/fuseLib.c)

truncate -s $(($SIZEFILE_FUSE - $SIZEBLOCK_FUSE)) $MPOINT/fuseLib.c
truncate -s $(($SIZEFILE_LINUX - $SIZEBLOCK_LINUX)) temp/fuseLib.c

#Check the integrity of the virtual disk again and perform a diff between the original
#file and the truncated one.

echo -e "Checking the integrity of the virtual disk\n"
if  $MY_FSCK $VDISK; then
        echo -e "${GREEN}The integrity of the virtual disk is OK${END_COLOR}";
else
	echo -e "${RED}The integrity of the virtual disk is not OK${END_COLOR}";

fi

read -p "Press enter..."

DIFF=$(diff $MPOINT/fuseLib.c ./temp/fuseLib.c -s)

echo -e "${YELLOW}COMPARISON BETWEEN TRUNCATED fuseLib.c IN EACH FS:\n$DIFF ${END_COLOR}\n"

#Copy a third text file into your FS.

echo -e "Copying MyFileSystem.c to $MPOINT"
cp ./src/MyFileSystem.c $MPOINT 

echo -e "Copying MyFileSystem.c to temp directory\n"
cp ./src/fuseLib.c ./src/MyFileSystem.c ./temp

#Check the integrity of the virtual disk and perform a diff between the original file
#and the one copied into the FS.

echo -e "Checking the integrity of the virtual disk\n"
if  $MY_FSCK $VDISK; then
        echo -e "${GREEN}The integrity of the virtual disk is OK${END_COLOR}";
else
        echo -e "${RED}The integrity of the virtual disk is not OK${END_COLOR}";
fi

read -p "Press enter..."

DIFF=$(diff $MPOINT/MyFileSystem.c ./temp/MyFileSystem.c -s)

echo -e "${YELLOW}COMPARISON BETWEEN MyFileSystem.c IN EACH FS:\n$DIFF ${END_COLOR}\n"

#Truncate the second file in the temp folder and in your FS, so as to increase the file
#size in one block.

echo -e "Increasing the size in one block of myFS.h in $MPOINT and in the temp directory \n"
SIZEBLOCK_FUSE=$(stat -c "%o" $MPOINT/myFS.c)
SIZEBLOCK_LINUX=$(stat -c "%o" ./temp/myFS.c)
SIZEFILE_FUSE=$(stat -c "%s" $MPOINT/myFS.c)
SIZEFILE_LINUX=$(stat -c "%s" temp/myFS.c)

truncate -s $(($SIZEFILE_FUSE + $SIZEBLOCK_FUSE)) $MPOINT/myFS.c
truncate -s $(($SIZEFILE_LINUX + $SIZEBLOCK_LINUX)) temp/myFS.c


#Check the integrity of the disk and perform a diff between the original file and the
#truncated one.

if $MY_FSCK $VDISK; then
        echo -e "${GREEN}The integrity of the virtual disk is OK${END_COLOR}";
else
        echo -e "${RED}The integrity of the virtual disk is not OK${END_COLOR}";

fi

 read -p "Press enter..."

DIFF=$(diff $MPOINT/myFS.h ./temp/myFS.h -s)

echo -e "${YELLOW}COMPARISON BETWEEN TRUNCATED myFS.h IN EACH FS:\n$DIFF ${END_COLOR}\n"

rm -rf temp
