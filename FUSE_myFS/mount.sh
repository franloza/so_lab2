#!/bin/bash
#@author: Francisco Jose Lozano Serrano

if [ ! -d mount-point ]; then
        mkdir mount-point;
fi

gnome-terminal -e "./fs-fuse -m -a $1 -f '-d -s mount-point'"
